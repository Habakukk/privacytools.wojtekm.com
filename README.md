[![privacytools](https://privacytools.io/assets/img/layout/logo.png)](https://www.privacytools.io/)

_Szyfrowanie przeciwko globalnej masowej inwigilacji._

[![Build Status](https://travis-ci.com/privacytoolsIO/privacytools.io.svg?branch=master)](https://travis-ci.com/privacytoolsIO/privacytools.io)

# Współtworzenie

Ważne jest, aby witryna taka jak privacytools.io była aktualna. Miej oko na aktualizacje oprogramowania wymienionych tutaj aplikacji. Śledź najnowsze wiadomości na temat dostawców, którzy są polecani. Staramy się nadążyć, ale nie jesteśmy idealni, a internet szybko się zmienia. Jeśli więc znajdziesz błąd lub uważasz, że dostawcy nie należy tutaj umieszczać, lub brakuje wykwalifikowanego dostawcy usług lub wtyczka przeglądarki nie jest już najlepszym wyborem lub cokolwiek innego ...

**Porozmawiaj z nami, proszę.** Join our [subreddit](https://www.reddit.com/r/privacytoolsIO/) i rozpocznij dyskusję. Jest to projekt społecznościowy, a naszym celem jest dostarczenie najlepszych dostępnych informacji w celu poprawy prywatności. Używamy również [/r/privacy](https://www.reddit.com/r/privacy). Dziękuję za udział.

Możesz przesłać swoje sugestie tutaj GitHub [(Issues)](https://github.com/privacytoolsIO/privacytools.io/issues) a także w naszym [subreddit](https://www.reddit.com/r/privacytoolsIO/). Proszę odnieść się do [Contributing Guidelines](.github/CONTRIBUTING.md) przed przesłaniem. Dziękuję Ci.

## Budowanie

1. Instalujemy [Ruby](https://www.ruby-lang.org/en/documentation/installation/)
1. Instalujemy [bundler](https://bundler.io/) by running `gem install bundler`.
1. Run `bundle install` to install the required dependencies.
1. Użyj `bundle exec jekyll build` aby, zbudować stronę internetową. Dane wyjściowe można znaleźć w `_site` katalogu. Podgląd na żywo jest również możliwy po uruchomieniu `bundle exec jekyll serve`

# Wsparcie privacytools.io

- [Dotacja.](https://privacytoolsio.github.io/privacytools.io/donate.html)
- [Spread the word.](https://privacytoolsio.github.io/privacytools.io/#participate)
- [Make suggestions on reddit.](https://www.reddit.com/r/privacytoolsIO/)
- Zobacz i edytuj kod źródłowy strony na Githubie.

# Tłumaczenia społeczności
- [繁体中文 / Chinese](https://privacytools.twngo.xyz/) - [GitHub](https://github.com/twngo/privacytools-zh)
- [Español / Spanish](https://victorhck.gitlab.io/privacytools-es/) - [GitLab](https://gitlab.com/victorhck/privacytools-es)
- [Deutsch / German](https://privacytools.it-sec.rocks/) - [GitHub](https://github.com/Anon215/privacytools.it-sec.rocks)
- [Italiano / Italian](https://privacytools-it.github.io/) - [GitHub](https://github.com/privacytools-it/privacytools-it.github.io)
- [Русский / Russian](https://privacytools.ru) - [GitHub](https://github.com/c0rdis/privacytools.ru)
- [Polski / Polska](https://privacytools.wojtekm.com/) - [GitHub](https://github.com/Habakukk/privacytools.wojtekm.com)

# Licencja
[Do What The Fuck You Want To Public License](https://github.com/privacytoolsIO/privacytools.io/blob/master/LICENSE.txt)
